import RPi.GPIO as GPIO
import time
import logging # TODO LOGGING

def setup_leds(ERROR_LED_PIN, READING_LED_PIN):
	GPIO.setup(ERROR_LED_PIN, GPIO.OUT)
	GPIO.setup(READING_LED_PIN, GPIO.OUT)
	GPIO.output(ERROR_LED_PIN, GPIO.LOW)
	GPIO.output(READING_LED_PIN, GPIO.LOW)

def reading_led(READING_LED_PIN, error_count):
	if (error_count == ERROR_LIMIT):
		error_led(ERROR_LED_PIN)
	GPIO.output(READING_LED_PIN, GPIO.HIGH)
	time.sleep(1)
	GPIO.output(READING_LED_PIN, GPIO.LOW)

def error_led(ERROR_LED_PIN):
	GPIO.output(ERROR_LED_PIN, GPIO.HIGH)