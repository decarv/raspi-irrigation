import RPi.GPIO as GPIO
import time
import Freenove_DHT as DHT
import logging # TODO LOGGING
from leds import reading_led
from savedata import save

def setup_dht11():
	pass

def read_dht(PIN):
	dht = DHT.DHT(PIN)
	read = None
	reading_time = time.now
	error_count = 0
	
	while (read != dht.DHT11_OK):
		read = dht.readDHT11()
		reading_led(READING_LED_PIN, error_count)
		error_count += 1
		time.sleep(2)

	return dht.humidity, dht.temperature