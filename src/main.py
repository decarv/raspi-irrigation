import time

import led
import dht11
import pcf8591
import RPi.GPIO as GPIO
import time
import Freenove_DHT as DHT
import logging # TODO LOGGING
from leds import reading_led
from savedata import save

def engine():
	setup_leds()
	setup_dht11()
	adc = setup_pcf8591()

	cnt = 0
	while(True):
		humidity, temperature = read_dht(DHT_PIN)
		light_intensity = read_photoresistor(adc, PHOTORESISTOR_CH)
		soil_humidity = read_soilhumidity(SOIL_PIN)

		if soil_humidity < SOIL_THRESHOLD:
			pump(adc)
			watered_last = datetime.datetime.now().isoformat()

		if loop_count % STOREFREQ == 0:
			storedata(
				datetime.datetime.now().isoformat(),
				humidity, 
				temperature, 
				light_intensity, 
				soil_humidity,
				watered_last)
			loop_count = 0
		
		time.sleep(300)
		loop_count += 1

if __name__ == '__main__':
	try:
		engine()
	except:
		GPIO.cleanup()
