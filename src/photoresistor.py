import pcf8591
from savedata import savedata

def photoresistor_read(adc, channel):
	reading = adc.analogRead(channel)
	light_intensity = (reading / 255)
	savedata({"light_intensity": light_intensity})
	return light_intensity
